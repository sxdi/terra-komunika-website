<?php namespace Terra\Core\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Teams Back-end Controller
 */
class Teams extends Controller
{
    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.ReorderController'
    ];

    /**
     * @var string Configuration file for the `FormController` behavior.
     */
    public $formConfig = 'config_form.yaml';


    /**
     * @var string Configuration file for the `FormController` behavior.
     */
    public $reorderConfig = 'config_reorder.yaml';


    /**
     * @var string Configuration file for the `ListController` behavior.
     */
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        $this->bodyClass = 'compact-container';
        BackendMenu::setContext('Terra.Core', 'core', 'teams');
    }
}
