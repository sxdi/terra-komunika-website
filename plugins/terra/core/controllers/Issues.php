<?php namespace Terra\Core\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Issues Back-end Controller
 */
class Issues extends Controller
{
    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    /**
     * @var string Configuration file for the `FormController` behavior.
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string Configuration file for the `ListController` behavior.
     */
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        $this->bodyClass = 'compact-container';
        parent::__construct();
        BackendMenu::setContext('Terra.Core', 'core', 'issues');
    }
}
