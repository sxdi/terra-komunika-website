<?php namespace Terra\Core;

use Backend;
use System\Classes\PluginBase;

/**
 * Core Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Core',
            'description' => 'No description provided yet...',
            'author'      => 'Terra',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        // return []; // Remove this line to activate

        return [
            'Terra\Core\Components\BaseContact' => 'BaseContact',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'terra.core.some_permission' => [
                'tab' => 'Core',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // return []; // Remove this line to activate

        return [
            'core' => [
                'label'       => 'Terra',
                'url'         => Backend::url('terra/core/services'),
                'icon'        => 'icon-leaf',
                'permissions' => ['terra.core.*'],
                'order'       => 500,
                'sideMenu'    => [
                    'services' => [
                        'label'       => 'Service',
                        'url'         => Backend::url('terra/core/services'),
                        'icon'        => 'icon-briefcase',
                        'permissions' => ['terra.core.*'],
                        'order'       => 500,
                    ],
                    'issues' => [
                        'label'       => 'Issue',
                        'url'         => Backend::url('terra/core/issues'),
                        'icon'        => 'icon-file',
                        'permissions' => ['terra.core.*'],
                        'order'       => 500,
                    ],
                    'teams' => [
                        'label'       => 'Team',
                        'url'         => Backend::url('terra/core/teams'),
                        'icon'        => 'icon-user',
                        'permissions' => ['terra.core.*'],
                        'order'       => 500,
                    ],
                    'partners' => [
                        'label'       => 'Partner',
                        'url'         => Backend::url('terra/core/partners'),
                        'icon'        => 'icon-comments',
                        'permissions' => ['terra.core.*'],
                        'order'       => 500,
                    ],
                    'careers' => [
                        'label'       => 'Career',
                        'url'         => Backend::url('terra/core/careers'),
                        'icon'        => 'icon-user-plus',
                        'permissions' => ['terra.core.*'],
                        'order'       => 500,
                    ],
                    'contacts' => [
                        'label'       => 'Inbox',
                        'url'         => Backend::url('terra/core/contacts'),
                        'icon'        => 'icon-envelope',
                        'permissions' => ['terra.core.*'],
                        'order'       => 500,
                    ],
                ]
            ],
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'strlimit' => [$this, 'makeStrLimit']
            ],
        ];
    }

    public function makeStrLimit($text)
    {
        return str_limit($text, 100, '...');
    }
}
