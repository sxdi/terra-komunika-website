<?php namespace Terra\Core\Components;

use Cms\Classes\ComponentBase;

class BaseContact extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseContact Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onSave()
    {
        $contact        = new \Terra\Contact\Models\Contact;
        $contact->email = post('email');
        $contact->name  = post('name');
        $contact->phone = post('phone');
        $contact->save();

        \Flash::success('Successfully save those form.');
        return \Redirect::refresh();
    }
}
