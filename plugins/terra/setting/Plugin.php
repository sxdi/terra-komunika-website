<?php namespace Terra\Setting;

use Backend;
use System\Classes\PluginBase;

/**
 * Setting Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Setting',
            'description' => 'No description provided yet...',
            'author'      => 'Terra',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        \Event::listen('cms.page.beforeDisplay', function($controller, $page, $url) {
            $controller->vars['settings'] = [
                'site'       => \Terra\Setting\Models\Site::instance(),
                'page'       => \Terra\Setting\Models\Page::instance(),
                'appearance' => \Terra\Setting\Models\Appearance::instance(),
                'blocker'    => \Terra\Setting\Models\Blocker::instance(),
                'heading'    => \Terra\Setting\Models\Heading::instance(),
            ];
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Terra\Setting\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'terra.setting.some_permission' => [
                'tab' => 'Setting',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'setting' => [
                'label'       => 'Setting',
                'url'         => Backend::url('terra/setting/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['terra.setting.*'],
                'order'       => 500,
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'sites' => [
                'label'       => 'Site',
                'description' => 'Manage available web site settings',
                'category'    => 'Web',
                'icon'        => 'icon-globe',
                'class'       => 'Terra\Setting\Models\Site',
                'order'       => 1,
            ],
            'appearances' => [
                'label'       => 'Appearance',
                'description' => 'Manage available web appearance settings',
                'category'    => 'Web',
                'icon'        => 'icon-image',
                'class'       => 'Terra\Setting\Models\Appearance',
                'order'       => 2,
            ],
            'blockers' => [
                'label'       => 'Blocker',
                'description' => 'Manage available web blocker settings',
                'category'    => 'Web',
                'icon'        => 'icon-laptop',
                'class'       => 'Terra\Setting\Models\Blocker',
                'order'       => 4,
            ],
            'headings' => [
                'label'       => 'Heading',
                'description' => 'Manage available web heading settings',
                'category'    => 'Web',
                'icon'        => 'icon-font',
                'class'       => 'Terra\Setting\Models\Heading',
                'order'       => 5,
            ],
        ];
    }
}
